package com.task.entity;

import java.io.Serializable;

public class User implements Serializable {
	private String userName;//用户名
	private String pwd;     //密码
	private String payName; //支付宝姓名
	private String payNum;  //支付宝账号
	private String phoneNum;//手机号
	private String qqNum;   //QQ账号
	public User() {
		super();
		// TODO Auto-generated constructor stub
	}
	public User(String userName, String pwd, String payName, String payNum,
			String phoneNum, String qqNum) {
		super();
		this.userName = userName;
		this.pwd = pwd;
		this.payName = payName;
		this.payNum = payNum;
		this.phoneNum = phoneNum;
		this.qqNum = qqNum;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getPwd() {
		return pwd;
	}
	public void setPwd(String pwd) {
		this.pwd = pwd;
	}
	public String getPayName() {
		return payName;
	}
	public void setPayName(String payName) {
		this.payName = payName;
	}
	public String getPayNum() {
		return payNum;
	}
	public void setPayNum(String payNum) {
		this.payNum = payNum;
	}
	public String getPhoneNum() {
		return phoneNum;
	}
	public void setPhoneNum(String phoneNum) {
		this.phoneNum = phoneNum;
	}
	public String getQqNum() {
		return qqNum;
	}
	public void setQqNum(String qqNum) {
		this.qqNum = qqNum;
	}
	
}
