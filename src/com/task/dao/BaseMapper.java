package com.task.dao;

import java.util.List;

import com.task.entity.Page;

/**
 * 通用mapper
 * @author: DuZiTeng
 * @email:  869688737@qq.com
 * @time:   2017-10-29下午7:46:51
 * @version:1.0
 */
public interface BaseMapper<T> {
	/**
	 * 添加单个对象
	 * @param entity 
	 * @return 
	 */
	public int insert(T entity);
	
	/**
	 * 修改单个对象
	 * @param entity
	 * @return
	 */
	public int update(T entity);
	
	/**
	 * 删除单个对象
	 * @param entity
	 * @return
	 */
	public int delete(T entity);
	
	/**
	 * 通过主键批量删除
	 * @param pks
	 * @return
	 */
	public int deleteList(String [] pks);
	
	/**
	 * 查询单个对象
	 * @param entity
	 * @return
	 */
	public T select(T entity);
	
	/**
	 * 通过关键字分页查询数据列表
	 * @param page
	 * @return
	 */
	public List<T> selectPageList(Page<T> page);
	
	/**
	 * 通过关键字分页查询，返回总记录数
	 * @param page
	 * @return
	 */
	public Integer selectPageCount(Page<T> page);
	
	/**
	 * 通过关键字分页查询数据列表
	 * @param page
	 * @return
	 */
	 public List<T> selectPageListUseDyc(Page<T> page);
		
		/**
		 * 通过关键字分页查询，返回总记录数
		 * @param page
		 * @return
		 */
	public Integer selectPageCountUseDyc(Page<T> page);
	
	
	

}
