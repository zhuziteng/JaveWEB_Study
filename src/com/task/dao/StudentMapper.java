package com.task.dao;

import com.task.entity.Student;

public interface StudentMapper extends BaseMapper<Student> {

}
