package com.task.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.task.dao.UserMapper;
import com.task.entity.Page;
import com.task.entity.User;
import com.task.service.UserService;
@Service("userService")
@Transactional
public class UserServiceimpl extends BaseServiceimpl<User> implements UserService {
	@Autowired
	private UserMapper mapper;
	@Override
	public int insert(User entity) throws Exception {
		
		return mapper.insert(entity);
	}

	@Override
	public int update(User entity) throws Exception {
		
		return mapper.update(entity);
	}

	@Override
	public int delete(User entity) throws Exception {
		
		return mapper.delete(entity);
	}

	@Override
	public int deleteList(String[] pks) throws Exception {
		
		return mapper.deleteList(pks);
	}

	@Override
	public User select(User entity) {
		
		return mapper.select(entity);
	}

	@Override
	public Page<User> selectPage(Page<User> page) {
		
		return (Page<User>) mapper.selectPageList(page);
	}

	@Override
	public Page<User> selectPageUseDyc(Page<User> page) {
		
		return (Page<User>) mapper.selectPageListUseDyc(page);
	}

}
