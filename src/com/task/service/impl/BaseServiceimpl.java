package com.task.service.impl;

import com.task.dao.BaseMapper;
import com.task.entity.Page;
import com.task.service.BaseService;

public class BaseServiceimpl<T> implements BaseService<T> {
	protected BaseMapper<T> baseMapper;
	@Override
	public int insert(T entity) throws Exception {
		
		return baseMapper.insert(entity);
	}

	@Override
	public int update(T entity) throws Exception {

		return baseMapper.update(entity);
	}

	@Override
	public int delete(T entity) throws Exception {
		
		return baseMapper.delete(entity);
	}

	@Override
	public int deleteList(String[] pks) throws Exception {
		
		return baseMapper.deleteList(pks);
	}

	@Override
	public T select(T entity) {
		
		return baseMapper.select(entity);
	}

	@Override
	public Page<T> selectPage(Page<T> page) {
		
		return (Page<T>) baseMapper.selectPageList(page);
	}

	@Override
	public Page<T> selectPageUseDyc(Page<T> page) {
		// TODO Auto-generated method stub
		return (Page<T>) baseMapper.selectPageListUseDyc(page);
	}

}
