package com.task.service;

import com.task.entity.Page;

/**
 * 通用的service
 * @author: DuZiTeng
 * @email:  869688737@qq.com
 * @time:   2017-10-29下午7:47:39
 * @version:1.0
 */
public interface BaseService<T> {
	/**
	 * 添加单个对象
	 * @param entity
	 * @return
	 * @throws Exception
	 */
	public int insert (T entity) throws Exception;
	
	/**
	 * 修改单个对象
	 * @param entity
	 * @return
	 * @throws Exception
	 */
	public int update(T entity) throws Exception;
	 
	/**
	 * 删除单个对象
	 * @param entity
	 * @return
	 * @throws Exception
	 */
	public int delete(T entity) throws Exception;
	
	/**
	 * 通过主键批量删除
	 * @param pks
	 * @return
	 * @throws Exception
	 */
	public int deleteList(String [] pks) throws Exception;
	
	
	/**
	 * 查询单个对象
	 * @param entity
	 * @return
	 */
	public T select(T entity);
	
	/**
	 * 通过关键字分页查询
	 * @param page
	 * @return
	 */
	public Page<T> selectPage(Page<T> page); 
	
	
	/**
	 * 通过多条件分页查询
	 * @param page
	 * @return
	 */
	public Page<T> selectPageUseDyc(Page<T> page); 
}
