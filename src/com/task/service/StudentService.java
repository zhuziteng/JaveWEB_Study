package com.task.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.task.dao.StudentMapper;
import com.task.entity.Student;
@Service("studentService")
@Transactional
public class StudentService {
	@Autowired
	private StudentMapper mapper;
	public void insert(){
		Student stu = new Student();
		stu.setId("2");
		stu.setName("jack");
		stu.setSex("男");
		mapper.insert(stu);
	}
}
