package com.task.controller;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.task.entity.Student;
import com.task.service.StudentService;

@Controller
public class StudentController {
	@Resource
	private StudentService service;
	@RequestMapping("test")
	@ResponseBody
	public Student test(){
		service.insert();
		Student stu = new Student();
		stu.setId("1");
		stu.setName("jack");
		stu.setSex("男");
		return stu;
	}
}
