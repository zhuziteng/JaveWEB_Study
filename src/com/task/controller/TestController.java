package com.task.controller;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class TestController {
	@RequestMapping("hello")
	public @ResponseBody Map test(){
		Map<String, String> map = new HashMap<String, String>();
		map.put("1", "20");
		map.put("name", "jack");
		return map;
	}
}
