package com.task.controller;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.task.entity.User;
import com.task.service.UserService;

@Controller
@RequestMapping("/user")
public class UserController {
	@Resource
	private UserService service;
	/**
	 * 用户注册
	 * @param user
	 * @param request
	 * @return
	 */
	@RequestMapping("/regist")
	public String userRegist(User user,HttpServletRequest request){
		try {
			service.insert(user);
			return "";//注册成功返回登录页
		} catch (Exception e) {
			request.setAttribute("mes", "注册失败请重新注册!");
			e.printStackTrace();
		}
		return "";
	}
	/**
	 * 用户登录
	 * @param user
	 * @param request
	 * @return
	 */
	@RequestMapping("/login")
	public String userLogin(User user,HttpServletRequest request){
		User selectUser = service.select(user);
		if(selectUser == null){
			//无此用户,不能登录
			return "";
		}
		request.getSession().setAttribute("user", selectUser);
		return ""; //登录成功,跳转至首页
	}
	/**
	 * 找回密码
	 * @param user
	 * @param request
	 * @return
	 */
	@RequestMapping("/findPwd")
	public String userFindPwd(User user,HttpServletRequest request){
		User selectUser = service.select(user);
		if(selectUser !=null){
			//跳转至重新设置密码页面
			return "";
		}
		request.setAttribute("msg", "用户不存在");
		return "";
	}
	/**
	 * 找回更新密码
	 * @param user
	 * @param request
	 * @return
	 */
	@RequestMapping("/findUpdatePwd")
	public String userUpdatePwd(User user,HttpServletRequest request){
		try {
			service.update(user);
			//密码找回成功,去到登录页面
			return "";
		} catch (Exception e) {
			//密码找回失败,去到找回密码页面
			e.printStackTrace();
			return "";
		}
	}
	/**
	 * 用户修改密码
	 * @return
	 */
	@RequestMapping("/updatePwd")
	public String userUpdatePwd(User user){
		try {
			service.update(user);
			//密码修改成功,重新登录
			return "";
		} catch (Exception e) {
			//密码修改失败
			e.printStackTrace();
			return "";
		}
	}
	
}
